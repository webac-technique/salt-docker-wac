/srv/httpd:
  file.directory:
    - user: root
    - group: root
    - mode: 777

/etc/httpd/conf/httpd.conf:
  file.managed:
    - source: salt://system/files/httpd.conf?saltenv=dev
