blih-conf:
  file.managed:
    - name: /usr/local/bin/blih
    - source: salt://system/files/blih?saltenv=dev
    - user: root
    - group: root
    - mode: 777