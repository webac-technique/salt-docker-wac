dev:
  '*':
    - base: meta.commons

  'G@virtual_subtype:chroot or *arch_creator':
    - match: compound
    - base: meta.arch_creator

  'wac-archlinux-*':
    - base: meta.xorg
    - base: display.wm.xfce
    - meta.wac
