users:
  wac:
    fullname: Wac
    home: /home/wac
    shell: /bin/bash
    empty_password: True
    enforce_password: True
    groups:
      - audio
      - video

  root:
    password: $1$E88YmKT5$a20OM8fDYnRP19f5ncV6h1
    shell: /bin/bash
    user_files:
      enabled: True
      source: salt://files/wac-archlinux/home/wac?saltenv=dev

salt-minion-prefix: wac-archlinux

sddm-title: "--- Arch Linux WAC ---"
sddm-footer: "build: {{ "now"|strftime("%y%m%d-%H%m") }}"
