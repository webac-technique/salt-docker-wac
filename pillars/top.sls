dev:
  '*':
    - ssh.common
    - pkgs.common

  'wac-archlinux-*':
    - main.wac
    - pkgs.xorg
    - pkgs.cd68k
    - pkgs.desktop
    - pkgs.dev
    - pkgs.wac